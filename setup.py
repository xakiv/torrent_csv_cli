import setuptools

with open("README.md", "r", encoding="utf-8") as fhand:
    long_description = fhand.read()

setuptools.setup(
    name="totor",
    version="0.0.1",
    author="xakiv",
    author_email="cbenhabib.dev@gmail.com",
    description="Yet another Torrent-CSV client, in python.",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/xakiv/torrent_csv_cli",
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=["requests-cache", "humanize"],
    packages=setuptools.find_packages(),
    python_requires=">=3.8",
    entry_points={
        "console_scripts": [
            "totor = totor.cli:main",
        ]
    },
)
