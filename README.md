Yet another Torrent-CSV client, in python.

This cli retreive data from the searchable [torrent.csv](https://git.torrents-csv.ml/heretic/torrents-csv-server) collaborative repositry.

## Installation
```
python3 -m venv venv
source venv/bin/activate
pip install git+https://gitlab.com/xakiv/torrent_csv_cli.git
totor "red dwarf" --size 5
(...results ...)
> Fetch next page ? [y/N]
```

## More information

```
totor --help
usage: totor [-h] [--size {1,2,3,4,5,6,7,8,9,10}] [--format {prompt,json}] [--trackers {best,all}] query

Looking for shared files matching a given query.

positional arguments:
  query                 The query used for the search request.

options:
  -h, --help            show this help message and exit
  --size {1,2,3,4,5,6,7,8,9,10}, -s {1,2,3,4,5,6,7,8,9,10}
                        How much results will be retreived per page. Max value will be 10. Default 10
  --format {prompt,json}, -f {prompt,json}
                        How the results will be formatted [json|prompt]. Default: prompt
  --trackers {best,all}, -t {best,all}
                        Select trackers listing [best|all]. Default: best
```