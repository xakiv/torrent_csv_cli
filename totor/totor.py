import pprint
from datetime import datetime, timedelta

import humanize
from requests_cache import CachedSession

pp = pprint.PrettyPrinter(indent=4)

session = CachedSession(expire_after=timedelta(days=1), backend="sqlite")


class APIError(Exception):
    pass


class NotFound(Exception):
    pass


def load_trackers(trackers_list):
    url = {
        "best": "https://raw.githubusercontent.com/ngosang/trackerslist/master/trackers_best.txt",
        "all": "https://raw.githubusercontent.com/ngosang/trackerslist/master/trackers_all.txt",
    }
    res = session.get(url[trackers_list])
    return "&tr=".join(
        [row.decode("utf-8") for row in filter(None, res.content.splitlines())]
    )


class Totor:
    TORRENT_CSV_SEARCH_URL = "https://torrents-csv.ml/service/search"

    def __init__(self, query, size, page, fmt, trackers_list):
        self.query = query
        self.size = size
        self.page = page
        self.format = fmt
        self.trackers = load_trackers(trackers_list)
        self.response = None

    def __iter__(self):
        return self

    def __next__(self):
        try:
            self.page += 1
            return self
        except Exception:
            raise StopIteration

    def handle_request(self):
        try:
            self.response = session.get(
                Totor.TORRENT_CSV_SEARCH_URL,
                params={"q": self.query, "size": self.size, "page": self.page},
            )
        except:
            raise APIError("API Request failed. ")

        if self.response.status_code != 200:
            raise APIError("API Request failed. ")
        if not self.response.json():
            raise NotFound(f"Query: {self.query} returned no results. ")

    def format_results(self):
        data = self.response.json()
        if self.format == "json":
            for row in data:
                row.update(
                    {
                        "magnet": f"magnet:?xt=urn:btih:{row['infohash']}&dn={row['name']}&tr={self.trackers}",
                        "size": humanize.naturalsize(row["size_bytes"]),
                    }
                )
            pp.pprint(data)
        else:
            for row in data:
                url = f"magnet:?xt=urn:btih:{row['infohash']}&dn={row['name']}&tr={self.trackers}"
                size = humanize.naturalsize(row["size_bytes"])
                created_at = datetime.utcfromtimestamp(row["created_unix"]).strftime(
                    "%Y-%m-%d %H:%M:%S"
                )
                print(f"Name: {row['name']}")
                print(f"Size: {size}")
                print(f"Created at: {created_at}")
                print(f"Seeders: {row['seeders']}")
                print(f"Leechers: {row['leechers']}")
                print(f"magnet URL: {url}")
                print(
                    "================================================================="
                )

    def __call__(self):
        self.handle_request()
        self.format_results()
