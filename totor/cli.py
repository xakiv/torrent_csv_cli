import argparse

from totor import Totor


def main():
    parser = argparse.ArgumentParser(
        description="Looking for shared files matching a given query. "
    )
    parser.add_argument(
        "query", type=str, help="The query used for the search request. "
    )
    parser.add_argument(
        "--size",
        "-s",
        type=int,
        choices=range(1, 11),
        default=10,
        help=(
            "How much results will be retreived per page. "
            "Max value will be 10. Default 10"
        ),
    )
    parser.add_argument(
        "--format",
        "-f",
        type=str,
        choices=["prompt", "json"],
        default="prompt",
        help=("How the results will be formatted [json|prompt]. Default: prompt"),
    )
    parser.add_argument(
        "--trackers",
        "-t",
        type=str,
        choices=["best", "all"],
        default="best",
        help=("Select trackers listing [best|all]. Default: best"),
    )
    args = parser.parse_args()

    for crawler in Totor(args.query, args.size, 1, args.format, args.trackers):
        crawler()
        fetch = input("--> Fetch next page ? [y/N]")
        if fetch.lower() in ["y", "yes", "yup", "true"]:
            continue
        else:
            break
    print("Done !")


if __name__ == "__main__":
    main()
